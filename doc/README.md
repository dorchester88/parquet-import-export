

This generates a series of scripts that will export data on a schema by schema basis from Vertica to S3
It also will load this exported data into the same schema. 

It expects the environment variables
    EXPORT_FORMAT 
        - 'csv' (not yet supported)
        - 'parquet'  (default)
        - 'orc' (not yet supported)

    SCHEMA_BUCKET
        - the s3 bucket in the format
        s3://bucket-name (not yet supported)


In preparation, you should have exported each schema into the ddl folder in the following format
   `schemaname_ddl.sql`
The program will retrieve the schema by parsing the files in the schema subdirectory and begin from there.

## PROCESS
1. insure that your .envrc is correct that environment variables are properly exported
2. run schema_pull.py against your cluster. resulting ddl files will be built into the ddl directory
3. 

