import vertica_python
import os
import sys
import logging


def run_parquet(fname):

    sql=[]
    fspec = 'export/'+fname+'_export.sql'
    sql = open(fspec,'r')
    # for this_line in infile:
    #     print(this_line)
    #     execute
    

    conn_info = {'host': os.getenv("DB_HOST"), 
        'port': os.getenv("DB_PORT"), 
        'user': os.getenv("DB_USERNAME"), 
        'password': os.getenv("DB_PASSWORD"), 
        'database': os.getenv("DB_DATABASE")}

    print("connection:", conn_info['host'])

        
    for cmd in sql:
        with vertica_python.connect(**conn_info) as conn:
            cur = conn.cursor()
            print(cmd.rstrip())
            try:
                cur.execute(cmd)
            except:
                # print('Query Failure', cmd)
                logging.error("SQL Query Failure")
                rec = 0
            else:
                rec = cur.fetchone()
            finally:
                logging.info('-----')
                logging.info(cmd.rstrip())
                logging.info("records exported: %s", rec)


fname = (sys.argv[1])
lname = 'log/'+fname+'.log'
logging.basicConfig(filename=lname, level=logging.INFO, format='%(asctime)s %(message)s')
run_parquet(fname)