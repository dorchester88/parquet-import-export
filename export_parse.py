import re
import sys
import os





def get_table(sql):
    m0 = re.search(r".+\.(?P<table>\w+)",sql)
    # print(m0)
    if m0 != None:
        m = m0.groupdict()
        # print(m)
        return m["table"]
    return 'No Table'

def parse_file(fname):
    ct = 0
    with open(fname) as file:
        lines = file.read()
        for this_line in lines:
            ct+=1
           # print(this_line)
        print(ct,'lines')

def get_schema(line):
    m0 = re.search(r"^[a-z]+",line)
    return m0[0]


def parse_import_lines(fname,schema):
    lct = 0
    infile = open('ddl/'+fname)
    for this_line in infile:
        m0 = re.search(r"CREATE TABLE",this_line)
        if m0 != None:
            lct+=1
            #print(this_line)

            table = get_table(this_line)
            target = s3_target(schema, table)
            print("COPY  "+target)
            outstring = "COPY  "+target

            source = schema+'.'+table+';'
            print("AS SELECT * FROM ", source)
            outstring += "\nAS SELECT * FROM ", source
    print(lct,'lines')
    outname = 'export/'+schema+'export.sql'
    outfile = open(outname, 'w')
    outfile.write(outstring)
    outfile.close()

def parse_export_lines(fname,schema):
    lct = 0
    infile = open('ddl/'+fname)
    for this_line in infile:
        lct+=1
        m0 = re.search(r"CREATE TABLE",this_line)
        if m0 != None:
            #print(this_line)
       
            table = get_table(this_line)
            target = s3_export_target(schema, table)
            print("EXPORT TO PARQUET ",target)
            outstring = "EXPORT TO PARQUET "+target

            source = schema+'.'+table+';'
            print("AS SELECT * FROM ",source)
            outstring+="\nAS SELECT * FROM "+source
    #print(lct,'lines')
    infile.close()
    outname = 'export/'+schema+'_export.sql'
    outfile = open(outname, 'w')
    outfile.write(outstring)
    outfile.close()

def s3_export_target(schema,table):
    bucket = os.getenv("S3_BUCKET")
    path = "(directory = '"+bucket+"/"+schema+"/"+table+"')"
    return path

def s3_import_target(schema,table):
    bucket = os.getenv("S3_BUCKET")
    path = "'"+bucket+"/"+schema+"/"+table+"/*')"
    return path
    
# parse_file('1.sql')

ddl_files = os.listdir('ddl/')
for fname in ddl_files:

    blah = get_schema(fname)
    print("schema: "+blah)
    parse_export_lines(fname, blah)

    


# fname = '1.sql'
# schema = (sys.argv[1])
# parse_export_lines(fname, schema)
# #parse_import_lines(fname, schema)



