import vertica_python
import os


def persist_file(fspec, stext):
        print(f"printing to file: {fspec}")
        # print(stext)
        file1 = open(fspec, 'w')
        
        for line in stext:
            file1.write(line[0])
            #file1.writelines(stext)
        
        file1.close()


def get_schemas():

    conn_info = {'host': os.getenv("DB_HOST"), 
        'port': os.getenv("DB_PORT"), 
        'user': os.getenv("DB_USERNAME"), 
        'password': os.getenv("DB_PASSWORD"), 
        'database': os.getenv("DB_DATABASE")}

    print("trying connection:", conn_info)


    with vertica_python.connect(**conn_info) as conn:
        cur = conn.cursor()
        cur.execute("SELECT schema_name FROM V_catalog.schemata where system_schema_creator is null;")
        s = cur.fetchall()
        print("schemata:", s)
        
    for schema in s:
        with vertica_python.connect(**conn_info) as conn:
            cur = conn.cursor()
            sql = "SELECT EXPORT_OBJECTS('', '" +schema[0]+ "', false)"
            # print(sql)
            cur.execute(sql)

            # print(cur.fetchall())
            schema_text = cur.fetchall()
            fspec = "ddl/"+schema[0]+"_ddl.sql"
            persist_file(fspec,schema_text)
 
    return schema_text


blah = get_schemas()



