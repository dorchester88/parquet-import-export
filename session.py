import vertica_python
import os
import boto3
import sys
import datetime
import logging


def get_session_token():
    # assumes aws profile is set with appropriate env vars
    sql = ['a','b','c','d']
    client = boto3.client('sts')
    j = client.get_session_token()

    creds = j['Credentials']
    access_key_id = creds['AccessKeyId']
    secret_access_key = creds['SecretAccessKey']
    session_token = creds['SessionToken']
    
    # sts session
    sql[0]= "ALTER SESSION SET AWSAuth = '"+access_key_id+":"+secret_access_key+"';"
    print(sql[0])
    sql[1] = "ALTER SESSION SET AWSSessionToken = '"+session_token+"';"
    print(sql[1])


    # storage location
    bucket = os.getenv("S3_BUCKET")
    cluster = os.getenv("DB_CLUSTER")
    
    sql[2] = "CREATE LOCATION '"+bucket+"' SHARED USAGE 'USER' LABEL 'exporter';"
    print(sql[2])

    sql[3] = "GRANT ALL ON LOCATION '"+bucket+"' TO dbadmin;"
    print(sql[3])
    return sql