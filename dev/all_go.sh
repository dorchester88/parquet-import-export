clear
cat ddl/archive_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py archive |tee export/archive_export.sql
python import_parse.py archive |tee import/archive_import.sql

clear
cat ddl/cleansed_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py cleansed |tee export/cleansed_export.sql
python import_parse.py cleansed |tee import/cleansed_import.sql

clear
cat ddl/customer_master_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py customer_master |tee export/customer_master_export.sql
python import_parse.py customer_master |tee import/customer_master_import.sql

clear
cat ddl/customer_ora_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py customer_ora |tee export/customer_ora_export.sql
python import_parse.py customer_ora |tee import/customer_ora_import.sql

clear
cat ddl/datamart_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py datamart |tee export/datamart_export.sql
python import_parse.py datamart |tee import/datamart_import.sql

clear
cat ddl/hcos_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py hcos |tee export/hcos_export.sql
python import_parse.py hcos |tee import/hcos_import.sql

clear
cat ddl/ingestion_reference_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py ingestion_reference |tee export/ingestion_reference_export.sql
python import_parse.py ingestion_reference |tee import/ingestion_reference_import.sql

clear
cat ddl/integration_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py integration |tee export/integration_export.sql
python import_parse.py integration |tee import/integration_import.sql

clear
cat ddl/mdm_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py mdm |tee export/mdm_export.sql
python import_parse.py mdm |tee import/mdm_import.sql

clear
cat ddl/metadata_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py metadata |tee export/metadata_export.sql
python import_parse.py metadata |tee import/metadata_import.sql

clear
cat ddl/outbound_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py outbound |tee export/outbound_export.sql
python import_parse.py outbound |tee import/outbound_import.sql

clear
cat ddl/payerspine_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py payerspine |tee export/payerspine_export.sql
python import_parse.py payerspine |tee import/payerspine_import.sql

clear
cat ddl/product_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py product |tee export/product_export.sql
python import_parse.py product |tee import/product_import.sql

clear
cat ddl/raw_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py raw |tee export/raw_export.sql
python import_parse.py raw |tee import/raw_import.sql

clear
cat ddl/reference_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py reference |tee export/reference_export.sql
python import_parse.py reference |tee import/reference_import.sql

clear
cat ddl/reporting_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py reporting |tee export/reporting_export.sql
python import_parse.py reporting |tee import/reporting_import.sql

clear
cat ddl/sandbox_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py sandbox |tee export/sandbox_export.sql
python import_parse.py sandbox |tee import/sandbox_import.sql

clear
cat ddl/sfa_history_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py sfa_history |tee export/sfa_history_export.sql
python import_parse.py sfa_history |tee import/sfa_history_import.sql

clear
cat ddl/tableau_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py tableau |tee export/tableau_export.sql
python import_parse.py tableau |tee import/tableau_import.sql

clear
cat ddl/teva_ingestion_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py teva_ingestion |tee export/teva_ingestion_export.sql
python import_parse.py teva_ingestion |tee import/teva_ingestion_import.sql

clear
cat ddl/teva_staging_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py teva_staging |tee export/teva_staging_export.sql
python import_parse.py teva_staging |tee import/teva_staging_import.sql

clear
cat ddl/teva_transform_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py teva_transform |tee export/teva_transform_export.sql
python import_parse.py teva_transform |tee import/teva_transform_import.sql

clear
cat ddl/veeva_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py veeva |tee export/veeva_export.sql
python import_parse.py veeva |tee import/veeva_import.sql

clear
cat ddl/wsmp_admin_ora_ddl.sql |grep 'CREATE TABLE' > 1.sql
python export_parse.py wsmp_admin_ora |tee export/wsmp_admin_ora_export.sql
python import_parse.py wsmp_admin_ora |tee import/wsmp_admin_ora_import.sql