

import os
import re
import boto3


def get_token():
    # assumes aws profile is set with appropriate env vars
    client = boto3.client('sts')
    j = client.get_session_token()

    creds = j['Credentials']
    access_key_id = creds['AccessKeyId']
    secret_access_key = creds['SecretAccessKey']
    session_token = creds['SessionToken']
    

    c1 = "ALTER SESSION SET AWSAuth = '"+access_key_id+":"+secret_access_key+"'"
    print(c1)
    c2 = "ALTER SESSION SET AWSSessionToken = '"+session_token+"'"
    print(c2)

def get_schema(fname):
    # this parses the schema name out of the filename
    m0 = re.search(r"(?P<schema>\w+)\_",fname)
    if m0 != None:
        m = m0.groupdict()
        # print(m)
        return m["schema"]
    return 'xx'
    

path = os.getcwd()+'/ddl'
print(path)

shortlist = os.listdir(path)
for fname in shortlist:
    print(fname)
    print(get_schema(fname))
    

get_token()



