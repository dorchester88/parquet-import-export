import re
import sys
import os



def get_table(sql):
    m0 = re.search(r".+\.(?P<table>\w+)",sql)
    # print(m0)
    if m0 != None:
        m = m0.groupdict()
        # print(m)
        return m["table"]
    return 'nonetable'

def parse_file(fname):
    ct = 0
    with open(fname) as file:
        lines = file.read()
        for this_line in lines:
            ct+=1
           # print(this_line)
        print(ct,'lines')

def get_schema(line):
    return 'archive'


def parse_import_lines(fname,schema):
    lct = 0
    infile = open(fname)
    for this_line in infile:
        lct+=1
            
        table = get_table(this_line)
        target = schema+'.'+table
        print "COPY  ",target


        source = s3_source(schema, table)
        print "FROM ", source, "PARQUET;"
    print(lct,'lines')

def parse_export_lines(fname,schema):
    lct = 0
    infile = open(fname)
    for this_line in infile:
        lct+=1
        #print(this_line)

       
        table = get_table(this_line)
        target = s3_export_target(schema, table)
        print "EXPORT TO PARQUET ",target

        source = schema+'.'+table+';'
        print "AS SELECT * FROM ", source
    print(lct,'lines')



def s3_source(schema,table):
    
    path = "'s3://teva-prod-schema-data-exports/"+schema+"/"+table+"/*'"
    return path
    
# env
export_format = os.environ.get['EXPORT_FORMAT']


fname = '1.sql'
schema = (sys.argv[1])
parse_import_lines(fname, schema)



# with open('test.sql') as file:
#     this_line = file.read()
#     get_table(this_line)
#     # if re.match('CREATE TABLE',this_line,0):
#     #     print(this_line)



