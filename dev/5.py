import re
import pandas as pd

def get_table(sql):
    m0 = re.match(r"(?P<schema>\w+)\.(?P<table>\w+)",sql)
    print(m0)
    if m0 != None:
        m = m0.groupdict()
        print(m)
        return m
    return ''

def parse_file(fname):
    ct = 0
    with open(fname) as file:
        lines = file.read()
        for this_line in lines:
            ct+=1
            print(this_line)
        print(ct,'lines')

def parse_lines(fname):
    lct = 0
    infile = open(fname)
    for this_line in infile:
        lct+=1
        print(this_line)
    print(lct,'lines')
    
# parse_file('1.sql')

parse_lines('1.sql')

# with open('test.sql') as file:
#     this_line = file.read()
#     get_table(this_line)
#     # if re.match('CREATE TABLE',this_line,0):
#     #     print(this_line)



