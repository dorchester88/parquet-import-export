import re
import sys
import pandas as pd



def get_table(sql):
    m0 = re.search(r".+\.(?P<table>\w+)",sql)
    # print(m0)
    if m0 != None:
        m = m0.groupdict()
        # print(m)
        return m["table"]
    return 'nonetable'

def parse_file(fname):
    ct = 0
    with open(fname) as file:
        lines = file.read()
        for this_line in lines:
            ct+=1
            print(this_line)
        print(ct,'lines')

def get_schema(line):
    return 'archive'


def parse_lines(fname):
    lct = 0
    infile = open(fname)
    for this_line in infile:
        lct+=1
        print(this_line)

        schema = get_schema(this_line)
        table = get_table(this_line)
        target = s3_target(schema, table)
        print(target)
    print(lct,'lines')

def s3_target(schema,table):
    path = 's3://teva-prod-data-blah-exports/'+schema+'/'+table
    return path
    
# parse_file('1.sql')
fname = (sys.argv[1])
parse_lines(fname)



# with open('test.sql') as file:
#     this_line = file.read()
#     get_table(this_line)
#     # if re.match('CREATE TABLE',this_line,0):
#     #     print(this_line)



